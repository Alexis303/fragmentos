package com.example.alexis.fragmentos;

import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements  PrimerFragment.OnFragmentInteractionListener, SegundaFragment.OnFragmentInteractionListener, TerceroFragment.OnFragmentInteractionListener {

    PrimerFragment primerFragment;
    SegundaFragment segundaFragment;
    TerceroFragment terceroFragment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        primerFragment= new PrimerFragment();

        getSupportFragmentManager().beginTransaction().add(R.id.marco, primerFragment).commit();


    }


    @Override
    public void onFragmentInteraction(String nFragment, String nEvento) {
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        segundaFragment = new SegundaFragment();
        terceroFragment = new TerceroFragment();

        if(nFragment.equals("BienvenidaFragment")){
            if(nEvento.equals("BUTTON_PRUEBA_CLICK")){



                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();

                transaction.replace(R.id.marco, segundaFragment);



                transaction.commit();
            }
        }

        if(nFragment.equals("SegundaFragment")){
            if(nEvento.equals("BUTTON_PRUEBA_CLICK")){



                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();

                transaction.replace(R.id.marco, terceroFragment);



                transaction.commit();
            }
        }










    }
















}
